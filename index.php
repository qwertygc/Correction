<?php
function sql($query, $array=array()) {
	global $bdd;
	$retour = $bdd->prepare($query);
	$retour->execute($array);
}
#http://snipplr.com/view/20671/short-url-function/
$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
 
function int_to_alph($int) {
	global $chrs;
	$alph = null;
	$base = sizeof($chrs);
	do {
		$alph = $chrs[($int % $base)] . $alph;
	}
	while($int = intval($int / $base));
	return $alph;
}
 function timeAgo($time) {
   $timestamp = time()-$time;
   if ($timestamp < 1)
       return 'Now';
   foreach (array(31104000 => 'year', 2592000 => 'month', 86400 => 'day', 3600 => 'hour', 60 => 'minute', 1 => 'second') as $secs => $str)
       if ($timestamp/$secs >= 1)
           return round($timestamp/$secs).' '.$str.((($timestamp/$secs) > 1)?'s':'').' ago';
}
function urlid() {
	$id = file_get_contents('inc/urlid');
	file_put_contents('inc/urlid', ++$id);
	return int_to_alph($id);
}
if(!is_dir('inc')) { mkdir('inc');}
$bdd = new PDO('sqlite:inc/database.db');
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if(!file_exists('inc/finediff.php')) {
	$bdd->query('CREATE TABLE "version" ("id" INTEGER PRIMARY KEY  NOT NULL ,"id_paste" TEXT DEFAULT (null) ,"time" INTEGER,"author" TEXT,"content" TEXT, "comment" TEXT);');
	file_put_contents('inc/finediff.php', file_get_contents('https://raw.githubusercontent.com/gorhill/PHP-FineDiff/master/finediff.php'));
	file_put_contents('inc/CriticParser.php', file_get_contents('https://raw.githubusercontent.com/qwertygc/CriticParser/master/CriticParser.php'));
	file_put_contents('inc/urlid', '0');
}
else {
	include 'inc/finediff.php';
	include 'inc/CriticParser.php';
}
if(isset($_POST['author'])) {setcookie('author', $_POST['author'], time() + 365*24*3600, null, null, false, true);}
error_reporting(-1);
?>
<!doctype html>
<html>
<head>
	<title>Correcteur</title>
	<style>
	body{width:90%;font-family:"DejaVu Sans", Verdana, Geneva, sans-serif;color:#333;background-color:#FFF;box-shadow:-4px 6px 9px rgba(50,50,50,0.5);margin:auto;padding:1em;}
	h1,h2,h3,h4,h5,h6{color:#616161;}
	h1 a,h1 a:hover{text-align:center;text-decoration:none; color:#616161;}
	textarea{overflow-x:hidden; width:100%;}
	.btn, input[type="submit"]{margin:auto;margin-top:1em;display:block;padding:10px;background-color:#fff;border:1px solid #ccc;color:#000;text-align:center;text-decoration:none;text-transform:uppercase;transform-style: flat;text-decoration:none;}
	table{border:1px solid #ccc; width:100%;}
	table th{border-bottom:1px solid #ccc;position:relative;}
	th,td{text-align:center;}
	.hash {font-size:.75em;display:inline;}
	.btn:hover,input[type="submit"]:hover{color:#333;background:#ccc;text-decoration:none;}
	input[type="text"],textarea{border: 1px solid #ccc;}
	input[type="text"],label{display:block;}
	a{color:#0C5B7A;text-decoration:underline;text-decoration-style:dotted;}
	a:hover,a:focus{color:#635182;text-decoration:underline;text-decoration-style:solid;}
	.criticbox {display: flex;}
	.box{border: 1px dashed #333;padding: 10px;margin:10px;box-sizing:border-box;vertical-align:top;flex: 1;padding: 1em;display:flex;flex-direction:column;}
	.box h1 {font-size:1.1em;}
	#result,#read,label,.help{color:#999;font-size:.75em;}
	ins{text-decoration:none;color:green;background:#dfd; display:inline;}
	del{text-decoration:none;color:red;background:#fdd; display:inline;}
	mark{text-decoration:none;background:#fff8ab;color:#666; display:inline;}
	.comment{color:#3498db;background:#a2c1d7; display:inline;}
	.critic{color:#ff8600;background:#f9d0a3; display:inline;}
	del.critic:after{content:" → ";color:#000}
	.raw-comment,.raw-highlight,.raw-del,.raw-sub,.raw-add{font-family:monospace}
	.raw-comment{color:blue}
	.raw-comment:before{content:"{>>"}
	.raw-comment:after{content:"<<}"}
	.raw-highlight{color:purple}
	.raw-highlight:before{content:"{=="}
	.raw-highlight:after{content:"==}"}
	.raw-del{color:red}
	.raw-del:before{content:"{--"}
	.raw-del:after{content:"--}"}
	.raw-sub{color:orange}
	.raw-sub:before{content:"{~~"}
	.raw-sub-before:after{content:"~> "}
	.raw-sub:after{content:"~~}"}
	.raw-add{color:green}
	.raw-add:before{content:"{++"}
	.raw-add:after{content:"++}"}
	.info {border-radius:10px; padding:10px; margin: 10px; background-color:transparent; background:#ec7471; border:red 2px solid; color:#fff; font-weight:bold; text-align:center;}
	.box a.close{color:#ccc;float:right;margin:0;padding:0;display:inline-block;}
	.box a.close:hover{color:#333;}
	</style>
</head>
<body>
	<h1><a href="?">Correcteur</a></h1>
<?php
$cookie = (isset($_COOKIE['author'])) ? $_COOKIE['author'] : 'Anonymous';
$time = time();
$diffbox = '';
if(isset($_GET['id'])) {
	$query = $bdd->prepare('SELECT * FROM version WHERE id_paste=:id ORDER BY time DESC');
	$query->execute(array('id'=>$_GET['id']));
	echo 'Share: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	echo '	<form method="get">
			<input type="hidden" name="id" value="'.$_GET['id'].'"/>
		<table>
			<tr>
				<th>#id</th>
				<th colspan="2"></th>
				<th>Permalink</th>
				<th>Date</th>
				<th>Author</th>
				<th>Comment</th>
			</tr>';
	while($data = $query->fetch()) {
		echo '
			<tr>
				<td>'.$data['id_paste'].'</td>
				<td><input type="radio" name="oldid" value="'.$data['id'].'" title="old version"></td>
				<td><input type="radio" name="newid" value="'.$data['id'].'" title="new version"></td>
				<td>
					<a href="?id='.$_GET['id'].'&amp;id_paste='.$data['id'].'" class="hash">'.md5($data['id']).'</a>
				</td>
				<td title="'.date('c', $data['time']).'">'.timeAgo($data['time']).'</td>
				<td>'.$data['author'].'</td>
				<td>'.$data['comment'].'</td>
			</tr>';
	}
	echo '
		</table>
		<input type="submit"/>
	</form>';
	if(isset($_GET['id_paste']) AND $_GET['id_paste'] != null) {
		$query = $bdd->prepare('SELECT * FROM version WHERE id=:id_paste');
		$query->execute(array('id_paste'=>$_GET['id_paste']));
		$data = $query->fetch();
		$content = $data['content'];
	}
	else {
		$query = $bdd->prepare('SELECT content FROM version WHERE id=(select MAX(id) FROM version WHERE id_paste=:id)');
		$query->execute(array('id'=>$_GET['id']));
		$data = $query->fetch();
		$content = $data['content'];
	}
	if(isset($_GET['newid']) && isset($_GET['oldid'])) {
		$queryold = $bdd->prepare('SELECT content FROM version WHERE id=:oldid');
		$queryold->execute(array('oldid'=>$_GET['oldid']));
		$dataold = $queryold->fetch();
		$querynew = $bdd->prepare('SELECT content FROM version WHERE id=:newid');
		$querynew->execute(array('newid'=>$_GET['newid']));
		$datanew = $querynew->fetch();
		$from_text =  CriticParser::rejectEdits($dataold['content']);
		$to_text = CriticParser::acceptEdits($datanew['content']);
		similar_text($from_text, $to_text, $percent);
		$diff_opcodes = FineDiff::getDiffOpcodes($from_text, $to_text, FineDiff::$wordGranularity);
		echo round($percent, 2).'%';
		echo '<div class="criticbox">';
		echo '	<div class="box" id="diff">
				<a href="javascript:void(0);" onclick="document.getElementById(\'diff\').style.display = \'none\';" class="close">&times;</a>
				<h1>Diff version</h1>
				'.nl2br(htmlspecialchars_decode(FineDiff::renderDiffToHTMLFromOpcodes($from_text, $diff_opcodes))).
			'</div>';		
		echo '	<div class="box"  id="marked">
				<a href="javascript:void(0);" onclick="document.getElementById(\'marked\').style.display = \'none\';" class="close">&times;</a>
				<h1>Marked version</h1>'
				.nl2br(htmlspecialchars_decode(CriticParser::renderEdits($content))).
			'</div>';
		echo '	<div class="box"  id="raw">
				<a href="javascript:void(0);" onclick="document.getElementById(\'raw\').style.display = \'none\';" class="close">&times;</a>
				<h1>Raw version</h1>'
				.nl2br(htmlspecialchars_decode(CriticParser::raw($content))).
			'</div>';	
		echo '	<div class="box"  id="wrong">
				<a href="javascript:void(0);" onclick="document.getElementById(\'wrong\').style.display = \'none\';" class="close">&times;</a>
				<h1>Wrong version</h1>'
				.nl2br(htmlspecialchars_decode(CriticParser::rejectEdits($content))).
			'</div>';		
		echo '	<div class="box"  id="corrected">
				<a href="javascript:void(0);" onclick="document.getElementById(\'corrected\').style.display = \'none\';" class="close">&times;</a>
				<h1>Corrected version</h1>'
				.nl2br(htmlspecialchars_decode(CriticParser::acceptEdits($content)))
			.'</div>';		
		echo '</div>';	
		 
	}
	if(isset($_POST['content'])) {
		$query = $bdd->prepare('SELECT time, content FROM version WHERE id_paste=? ORDER BY time DESC LIMIT 1');
		$query->execute(array($_GET['id']));
		$data = $query->fetch();
		if($_POST['last_id'] != $data['time']) {
			echo '<div class="info">an new version has been published in the meanwhile</div>';
			$content = $_POST['content'];
			echo '<div class="criticbox">';
			echo '	<div class="box" id="diff">
					<a href="javascript:void(0);" onclick="document.getElementById(\'diff\').style.display = \'none\';" class="close">&times;</a>
					<h1>Diff version</h1>
					'.nl2br(htmlspecialchars_decode(FineDiff::renderDiffToHTMLFromOpcodes($from_text, $diff_opcodes))).
				'</div>';		
			echo '	<div class="box"  id="marked">
					<a href="javascript:void(0);" onclick="document.getElementById(\'marked\').style.display = \'none\';" class="close">&times;</a>
					<h1>Marked version</h1>'
					.nl2br(htmlspecialchars_decode(CriticParser::renderEdits($content))).
				'</div>';
			echo '	<div class="box"  id="raw">
					<a href="javascript:void(0);" onclick="document.getElementById(\'raw\').style.display = \'none\';" class="close">&times;</a>
					<h1>Raw version</h1>'
					.nl2br(htmlspecialchars_decode(CriticParser::raw($content))).
				'</div>';	
			echo '	<div class="box"  id="wrong">
					<a href="javascript:void(0);" onclick="document.getElementById(\'wrong\').style.display = \'none\';" class="close">&times;</a>
					<h1>Wrong version</h1>'
					.nl2br(htmlspecialchars_decode(CriticParser::rejectEdits($content))).
				'</div>';		
			echo '	<div class="box"  id="corrected">
					<a href="javascript:void(0);" onclick="document.getElementById(\'corrected\').style.display = \'none\';" class="close">&times;</a>
					<h1>Corrected version</h1>'
					.nl2br(htmlspecialchars_decode(CriticParser::acceptEdits($content)))
				.'</div>';		
			echo '</div>';	
		}
		else {
			sql('INSERT INTO `version`(`id_paste`,`time`,`author`,`content`, `comment`) VALUES (?, ?, ?, ?, ?)', array($_POST['id_paste'], time(), $_POST['author'], $_POST['content'], $_POST['comment']));
			header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			echo '<script>window.location.replace("http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'");</script>';
			die;
		}
	}
	$query = $bdd->prepare('SELECT time, content FROM version WHERE id_paste=? ORDER BY time DESC LIMIT 1');
	$query->execute(array($_GET['id']));
	$data = $query->fetch();
	echo '
	<form method="post">
		<label for="author">Author Name</label>
		<input type="text" name="author" id="author" value="'.$cookie.'"/>
		<span id="result"></span><span id="read"></span><span class="help">We use <a href="http://criticmarkup.com">CriticMarkup</a> for correcting our text. Please to <a href="http://criticmarkup.com/users-guide.php">read</a> the user guide!
		<ul>
			<li>Addition {++ ++}</li>
			<li>Deletion {-- --}</li>
			<li>Substitution {~~ ~> ~~}</li>
			<li>Comment {>> <<}</li>
			<li>Highlight {== ==}{>> <<}</li>
		</ul>
		</span>
		<label for="content">Content</label>
		<textarea id="content" name="content" rows="25">'.$content.'</textarea>
		<label for="comment">Comment</label>
		<textarea id="comment" name="comment" rows="5"></textarea>
		<input type="hidden" id="id_paste" name="id_paste" value="'.$_GET['id'].'">
		<input type="hidden" id="last_id" name="last_id" value="'.$data['time'].'">
		<input type="submit">
	</form>';
}
elseif(isset($_GET['admin'])) {
	$query = $bdd->query('SELECT id_paste, COUNT(*) FROM version GROUP BY id_paste ORDER BY COUNT(*) DESC');
	echo '<ul>';
	while($data = $query->fetch()) {
		echo '<li><a href="?id='.$data['id_paste'].'">'.$data['id_paste'].'</a></li>';
	}
	echo '</ul>';
}
else {
	echo '<a href="?id='.urlid().'" class="btn btn-default">click me</a>';
}
?>

	<script>
		var text = document.getElementById("content");
		text.onkeyup = function() {
		   var chaine = document.getElementById('content').value; 
			nb = chaine.split(/\b\w+\b/).length-1; 
			document.getElementById('content').value = chaine;
			document.getElementById("result").innerHTML = nb+' words ';
			document.getElementById("read").innerHTML =Math.floor(nb/200)+'mn ';
		}
	</script>
</body>
</html>